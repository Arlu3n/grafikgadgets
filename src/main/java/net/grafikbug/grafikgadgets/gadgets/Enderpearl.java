package net.grafikbug.grafikgadgets.gadgets;

import net.grafikbug.grafikgadgets.GrafikGadgets;
import net.grafikbug.grafikgadgets.utils.ItemBuilder;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

/**
 * Erstellt von lukas am 30.01.2018
 * © 2016-2017 Arlu3n | Alle Rechte vorbehalten
 */

public class Enderpearl extends Gadget {

    public Enderpearl(){
        super(new ItemBuilder(new ItemStack(Material.ENDER_PEARL)).setName("§6Enderperle").build());
    }

    @Override
    public boolean isAllowed(Player player) {
        return player.hasPermission("gadgets.enderpearl");
    }

    private final ItemStack ENDERPEARL = new ItemBuilder(new ItemStack(Material.ENDER_PEARL)).setName("§6Enderperle").build();

    @Override
    public void equip(Player player) {
        player.getInventory().setItem(GADGET_SLOT, ENDERPEARL);
    }

    @Override
    public void unequip(Player player) {
        player.getInventory().remove(ENDERPEARL);
    }

    private ArrayList<String> damaged = new ArrayList<>();

    @EventHandler
    public void onPort(ProjectileLaunchEvent event){
        if(!(event.getEntity() instanceof Enderpearl))
            return;

        if(!(event.getEntity().getShooter() instanceof Player))
            return;

        Player player = (Player)event.getEntity().getShooter();
        damaged.add(player.getUniqueId().toString());
        new BukkitRunnable(){
            @Override
            public void run() {
                if(!event.getEntity().isDead()){
                    event.getEntity().getWorld().playEffect(event.getEntity().getLocation(), Effect.CLOUD, 5);
                }else{
                    equip(player);
                    cancel();
                }
            }
        }.runTaskTimer(GrafikGadgets.getPlugin(), 10, 10);

    }

    @EventHandler
    public void onDamage(EntityDamageEvent event){
        if(!(event.getEntity() instanceof Player))
            return;

        Player player = (Player)event.getEntity();

        if(event.getCause() != EntityDamageEvent.DamageCause.FALL || !damaged.contains(player.getUniqueId().toString()))
            return;
        damaged.remove(player.getUniqueId().toString());
        event.setCancelled(true);
    }
}
