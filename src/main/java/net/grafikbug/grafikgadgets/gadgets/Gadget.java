package net.grafikbug.grafikgadgets.gadgets;

import net.grafikbug.grafikgadgets.GrafikGadgets;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

/**
 * Erstellt von lukas am 30.01.2018
 * © 2016-2017 Arlu3n | Alle Rechte vorbehalten
 */

public abstract class Gadget implements Listener{

    protected final int GADGET_SLOT = 8;

    private ItemStack itemStack;

    public Gadget(ItemStack itemStack){
        this.itemStack = itemStack;
        GrafikGadgets.getPlugin().getServer().getPluginManager().registerEvents(this, GrafikGadgets.getPlugin());
    }

    public abstract boolean isAllowed(Player player);

    public void use(Player player){
        if(isAllowed(player))
            equip(player);
    }

    public abstract void equip(Player player);
    public abstract void unequip(Player player);

    public ItemStack getItemStack(){
        return itemStack;
    }

}
