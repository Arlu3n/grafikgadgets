package net.grafikbug.grafikgadgets.commands;

import net.grafikbug.grafikgadgets.GrafikGadgets;
import net.grafikbug.grafikgadgets.inventory.MenuInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Erstellt von lukas am 31.01.2018
 * © 2016-2017 Arlu3n | Alle Rechte vorbehalten
 */

public class GadgetCommand implements CommandExecutor{

    public GadgetCommand(GrafikGadgets plugin){
        plugin.getCommand("gadget").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player))
            return true;

        Player player = (Player)sender;
        MenuInventory.open(player);

        return true;
    }

}
