package net.grafikbug.grafikgadgets.inventory;

import net.grafikbug.grafikgadgets.GrafikGadgets;
import net.grafikbug.grafikgadgets.gadgets.Enderpearl;
import net.grafikbug.grafikgadgets.gadgets.Gadget;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * Erstellt von lukas am 31.01.2018
 * © 2016-2017 Arlu3n | Alle Rechte vorbehalten
 */

public class MenuInventory implements Listener {

    private static String title = "§6Inventar";
    private static Inventory inventory = Bukkit.createInventory(null, 18, title);
    private static HashMap<Integer, Gadget> slots = new HashMap<>();

    static{
        slots.put(0, new Enderpearl());
        Iterator<Entry<Integer, Gadget>> it = slots.entrySet().iterator();

        while(it.hasNext()){
            Entry<Integer, Gadget> entry = it.next();
            inventory.setItem(entry.getKey(), entry.getValue().getItemStack());
        }
    }

    public static void open(Player player){
        new BukkitRunnable(){
            @Override
            public void run() {
                player.openInventory(inventory);
                cancel();
            }
        }.runTaskTimer(GrafikGadgets.getPlugin(), 10, 10);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event){
        if(event.getClickedInventory() == null || event.getCurrentItem() == null || !event.getClickedInventory().getTitle().equals(title))
            return;
        event.setCancelled(true);
        if(!slots.containsKey(event.getSlot()))
            return;
        slots.get(event.getSlot()).use((Player)event.getWhoClicked());
    }

}
