package net.grafikbug.grafikgadgets;

import net.grafikbug.grafikgadgets.commands.GadgetCommand;
import net.grafikbug.grafikgadgets.inventory.MenuInventory;
import org.bukkit.entity.EnderPearl;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Erstellt von lukas am 30.01.2018
 * © 2016-2017 Arlu3n | Alle Rechte vorbehalten
 */

public class GrafikGadgets extends JavaPlugin {

    private static GrafikGadgets plugin;
    private static GadgetCommand gadgetCommand;

    @Override
    public void onEnable(){
        plugin = this;
        gadgetCommand = new GadgetCommand(this);
        getServer().getPluginManager().registerEvents(new MenuInventory(), this);
    }

    @Override
    public void onDisable(){

    }

    public static GrafikGadgets getPlugin() {
        return plugin;
    }

}
