package net.grafikbug.grafikgadgets.utils;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Erstellt von lukas am 30.01.2018
 * © 2016-2017 Arlu3n | Alle Rechte vorbehalten
 */

public class ItemBuilder {

    private ItemStack itemStack;
    private ItemMeta itemMeta;

    public ItemBuilder(ItemStack itemStack){
        this.itemStack = itemStack;
        this.itemMeta = itemStack.getItemMeta();
    }

    public ItemBuilder setName(String name){
        itemMeta.setDisplayName(name);
        return this;
    }

    public ItemBuilder setLore(String...lore){
        itemMeta.setLore(Arrays.asList(lore));
        return this;
    }

    public ItemStack build(){
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

}
